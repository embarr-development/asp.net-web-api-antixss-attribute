﻿using Embarr.Testing.WebAPI;
using Embarr.WebAPI.AntiXss.AcceptanceTests.Services;
using Embarr.WebAPI.AntiXss.AcceptanceTests.WebApi.App_Start;
using TechTalk.SpecFlow;

namespace Embarr.WebAPI.AntiXss.AcceptanceTests.Hooks
{
    [Binding]
    public class WebApiHooks
    {
        [BeforeFeature("DefaultHost")]
        public static void BeforeDefaultHostFeature()
        {
            Host.StartHost<DefaultHost>(getBaseAddress: SetBaseAddress);
        }

        [AfterFeature("DefaultHost")]
        public static void AfterDefaultHostFeature()
        {
            Host.StopHost();
        }

        private static void SetBaseAddress(string baseAddress)
        {
            FeatureContextService.SaveValue("baseAddress", baseAddress);
        } 
    }
}