﻿using System.Net;
using Embarr.WebAPI.AntiXss.AcceptanceTests.Services;
using Embarr.WebAPI.AntiXss.AcceptanceTests.WebApi.Models;
using RestSharp;
using Should;
using TechTalk.SpecFlow;

namespace Embarr.WebAPI.AntiXss.AcceptanceTests.Steps
{
    [Binding]
    public class AntiXssAttributeSteps
    {
        [When(@"I pass '(.*)' to the Name property")]
        public void WhenIPassToProperty(string value)
        {
            MakeRequest(new TestModel { Name = value});
        }

        [When(@"I pass '(.*)' to the Description property")]
        public void WhenIPassToTheDescriptionProperty(string description)
        {
            MakeRequest(new TestModel { Description = description });
        }

        [When(@"I pass '(.*)' to the NoSlashesOrQuotes property")]
        public void WhenIPassToTheNoSlashesProperty(string value)
        {
            MakeRequest(new TestModel { NoSlashesOrQuotes = value });
        }

        [When(@"I pass '(.*)' to the CustomError property")]
        public void WhenIPassToTheCustomErrorProperty(string value)
        {
            MakeRequest(new TestModel { CustomError = value });
        }

        [When(@"I pass '(.*)' to the ResourceCustomError property")]
        public void WhenIPassToTheResourceCustomErrorProperty(string value)
        {
            MakeRequest(new TestModel { ResourceCustomError = value });
        }

        [Then(@"I receive the status code '(.*)'")]
        public void ThenIReceiveTheStatusCode(int statusCode)
        {
            var response = ScenarioContextService.GetValue<IRestResponse>();
            response.StatusCode.ShouldEqual((HttpStatusCode)statusCode);
        }

        [Then(@"I receive the error message '(.*)'")]
        public void ThenIReceiveThePropertyErrorMessage(string expectedMessage)
        {
            var responseBody = ScenarioContextService.GetValue<string>("validationResponse");
            responseBody.ShouldContain(expectedMessage);
        }

        private static void MakeRequest(TestModel testModel)
        {
            var restClient = new RestClient(FeatureContextService.GetValue<string>("baseAddress"));
            restClient.DefaultParameters.Clear();
            var restRequest = new RestRequest("api/test", Method.POST);

            restRequest.RequestFormat = DataFormat.Json;
            restRequest.AddHeader("content-type", "application/json");
            restRequest.AddBody(testModel);
            var response = restClient.Execute(restRequest);
            ScenarioContextService.SaveValue(response);
            ScenarioContextService.SaveValue("validationResponse", response.Content);
        }
    }
}